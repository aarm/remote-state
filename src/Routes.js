import React from 'react';
import { Route, Routes } from "react-router";
import Truck from "./truck/truck";

const RoutesApp = ({ }) => {

	return (
		<Routes>
			<Route path="/" element={<Truck />} />
		</Routes>
	);
};

export default RoutesApp;
