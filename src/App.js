import './App.css';
import {
    BrowserRouter as Router,
} from "react-router-dom";
import React from "react";
import RoutesApp from './Routes';

function App() {
  return (
      <Router>
          <div >
              <div className="content">
                  <div  className="container-fluid p-0">
                      <RoutesApp />
                  </div>
              </div>
          </div>
      </Router>
  );
}

export default App;
