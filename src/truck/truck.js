import React, {useEffect, useState} from "react";
import Map from "../map/map";
import axios from 'axios';
import {act} from "@testing-library/react";
const Truck = () => {

    const [truckList, setTruckList] = useState([])
    const [runningTruckList, setRunningTruckList] = useState([])
    const [stopTruckList, setStopTruckList] = useState([])
    const [activeMarkerList, setActiveMarkerList] = useState([])
    const [stopMarkerList, setStopMarkerList] = useState([])
    const [active, setActive] = useState('')

    useEffect(() => {
        getData()
        setActive('1')
    }, [])

    useEffect(() => {
        getRunningTruckList()
        getStopTruckList()
        getMarkerList()
    }, [truckList])


    useEffect(() => {
        getMarkerList()
    }, [active])

    const getMarkerList = () => {
        if (active == '1') {
            for (let i=0; i<truckList.length; i++) {
                let list = []
                if (truckList[i].lastRunningState.truckRunningState == 0) {
                    list = [truckList[i].lastRunningState.lat,  truckList[i].lastRunningState.lng]
                    stopMarkerList.push(list)
                }
            }
            setStopMarkerList([...stopMarkerList])
            for (let i=0; i<truckList.length; i++) {
                let list = []
                if (truckList[i].lastRunningState.truckRunningState == 1) {
                    list = [truckList[i].lastRunningState.lat,  truckList[i].lastRunningState.lng]
                    activeMarkerList.push(list)
                }
            }
            setActiveMarkerList([...activeMarkerList])

        } else if (active == '2') {
            for (let i=0; i<truckList.length; i++) {
                let list = []
                if (truckList[i].lastRunningState.truckRunningState == 1) {
                    list = [truckList[i].lastRunningState.lat,  truckList[i].lastRunningState.lng]
                    activeMarkerList.push(list)
                }
            }
            setActiveMarkerList([...activeMarkerList])
            setStopMarkerList([])

        } else if (active == '3') {
            for (let i=0; i<truckList.length; i++) {
                let list = []
                if (truckList[i].lastRunningState.truckRunningState == 0) {
                    list = [truckList[i].lastRunningState.lat,  truckList[i].lastRunningState.lng]
                    stopMarkerList.push(list)
                }
            }
            setStopMarkerList([...stopMarkerList])
            setActiveMarkerList([])
        } else {
            setStopMarkerList([])
            setActiveMarkerList([])
        }
    }

    const getRunningTruckList = () => {
        let list = []
        for (let i=0; i<truckList.length; i++) {
            if (truckList[i].lastRunningState.truckRunningState == 1) {
                list.push(truckList[i])
            }
        }
        setRunningTruckList([...list])
    }

    const getStopTruckList = () => {
        let list = []
        for (let i=0; i<truckList.length; i++) {
            if (truckList[i].lastRunningState.truckRunningState == 0) {
                list.push(truckList[i])
            }
        }
        setStopTruckList([...list])
    }

    const getData = () => {
        let base_url = 'https://api.mystral.in/tt/mobile/logistics/searchTrucks?auth-company=PCH&companyId=33&deactivated=false&key=g2qb5jvucg7j8skpu5q7ria0mu&q-expand=true&q-include=lastRunningState,lastWaypoint'
        axios.get(base_url).then(res => {
            setTruckList([...res.data.data])
        }).catch(error => {
            console.log(error)
        })
    }

    const getActive = (value) => {
        setActive(value)
    }

    function convertMS( milliseconds ) {
        var day, hour, minute, seconds;
        seconds = Math.floor(milliseconds / 1000);
        minute = Math.floor(seconds / 60);
        seconds = seconds % 60;
        hour = Math.floor(minute / 60);
        minute = minute % 60;
        day = Math.floor(hour / 24);
        hour = hour % 24;
        return {
            day: day,
            hour: hour,
            minute: minute,
            seconds: seconds
        };
    }


    return (
        <>
            <div className=''>
                <div className='row border-bottom'>
                    <div className={'col-sm-3 text-center ' + (active == '1' ? 'active' : '')} role='button' onClick={event => getActive('1')}>
                        <strong>Total Trucks</strong>
                        <br/>
                        <span>{truckList.length}</span>
                    </div>
                    <div className={'col-sm-3 text-center ' + (active == '2' ? 'active' : '')} role='button' onClick={event => getActive('2')}>
                        <strong>Running Trucks</strong>
                        <br/>
                        <span>{runningTruckList.length}</span>
                    </div>
                    <div className={'col-sm-3 text-center ' + (active == '3' ? 'active' : '')} role='button' onClick={event => getActive('3')}>
                        <strong>Stopped Trucks</strong>
                        <br/>
                        <span>{stopTruckList.length}</span>
                    </div>
                    <div className={'col-sm-3 text-center ' + (active == '4' ? 'active' : '')} role='button' onClick={event => getActive('4')}>
                        <strong>Idle Trucks</strong>
                        <br/>
                        <span>0</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-sm-3 pr-0 trucklist'>
                        {
                            truckList.length && active == '1' && truckList.map((value, index) => {
                                return <div className='border-bottom'>
                                    <div><h5>{value?.truckNumber} <span><img className='truckImg' src='https://image.shutterstock.com/image-vector/truck-icon-vector-illustration-on-260nw-390772729.jpg'/></span></h5></div>
                                    <div className='gray-text'>{value.lastRunningState.truckRunningState == 1 ? `Running since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr` : `Stopped since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr`}</div>
                                </div>
                            })
                        }
                        {
                            runningTruckList.length && active == '2' && runningTruckList.map((value, index) => {
                                return <div className='border-bottom'>
                                    <div><h5>{value?.truckNumber} <span><img className='truckImg' src='https://image.shutterstock.com/image-vector/truck-icon-vector-illustration-on-260nw-390772729.jpg'/></span></h5></div>
                                    <div className='gray-text'>{value.lastRunningState.truckRunningState == 1 ? `Running since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr` : `Stopped since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr`}</div>
                                </div>
                            })
                        }
                        {
                            stopTruckList.length && active == '3' && stopTruckList.map((value, index) => {
                                return <div className='border-bottom'>
                                    <div><h5>{value?.truckNumber} <span><img className='truckImg' src='https://image.shutterstock.com/image-vector/truck-icon-vector-illustration-on-260nw-390772729.jpg'/></span></h5></div>
                                    <div className='gray-text'>{value.lastRunningState.truckRunningState == 1 ? `Running since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr` : `Stopped since last ${convertMS(value.lastRunningState.stopStartTime).day}Days ${convertMS(value.lastRunningState.stopStartTime).hour} hr`}</div>
                                </div>
                            })
                        }
                    </div>
                    <div className='col-sm-9 p-0'>
                        <Map activeMarker={activeMarkerList} stopMarker={stopMarkerList}/>
                    </div>
                </div>
            </div>
        </>
    );
};

export default (Truck);
