import React from "react";
import { MapContainer, TileLayer, Marker, Popup , useMap} from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import L from "leaflet";
import carImgSelected from "./carnew.png";
import carImgRedSelected from "./car.png";
const position = [ 30.757902145385742, 76.13243865966797]

var greenCar = new L.icon({
    iconUrl: carImgSelected,
    iconRetinaUrl: carImgSelected,
    iconSize: [46, 38],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14],
});

var redCar = new L.icon({
    iconUrl: carImgRedSelected,
    iconRetinaUrl: carImgRedSelected,
    iconSize: [46, 38],
    iconAnchor: [9, 21],
    popupAnchor: [0, -14],
});


const Map = ({activeMarker, stopMarker}) => {

    function ChangeMap({ center, zoom }) {
        const map = useMap();
        map.setView(center, zoom);


        return null;
    }


    return (
        <>
            <MapContainer className="Map" center={position} zoom={13}>
                <ChangeMap zoom={13} center={activeMarker.length ? activeMarker[0] : position} />
                <TileLayer
                    url='https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}'
                    subdomains={['mt1','mt2','mt3']}
                />
                {
                    activeMarker.length && activeMarker.map(val => {
                        return <Marker position={val} icon={greenCar}></Marker>
                    })
                }
                {
                    stopMarker.length && stopMarker.map(val => {
                        return <Marker position={val} icon={redCar}></Marker>
                    })
                }
            </MapContainer>
        </>
    );
};

export default Map;
